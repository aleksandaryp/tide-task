
# Hello World Python Application

This repository contains a simple Python application that prints "Hello World" followed by the name of the environment it's running in. It's designed to demonstrate a basic CI/CD pipeline using GitLab for three different environments: main, test, and production.

## Prerequisites

- Python 3.x
- Git
- Access to a GitLab repository
