FROM python:3.9-slim

WORKDIR /app
COPY . /app
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 5000
# Create a non-root user and group with no password
RUN groupadd -r appgroup && useradd --no-log-init -r -g appgroup appuser
# Change the ownership of the /app folder to this new user
RUN chown -R appuser:appgroup /app
# Switch to the non-root user for security reasons
USER appuser

CMD ["python", "./hello.py"]